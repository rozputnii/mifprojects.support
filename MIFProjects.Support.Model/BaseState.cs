﻿using System;
using System.ComponentModel;

namespace MIFProjects.Support.Model
{
    public enum BaseState
    {
        [Description("Waiting for Staff Response")] WaitingForStaffResponse,
        [Description("Waiting for Customer")] WaitingForCustomer,
        [Description("On Hold")] OnHold,
        [Description("Cancelled")] Cancelled,
        [Description("Completed")] Completed
    }

    public static class ExtensionEnum
    {
        public static string GetDescription(this Enum value)
        {
            var fi = value.GetType().GetField(value.ToString());

            var attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (attributes.Length > 0)
                return attributes[0].Description;
            return value.ToString();
        }
    }
}