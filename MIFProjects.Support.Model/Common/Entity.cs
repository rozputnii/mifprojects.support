using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MIFProjects.Support.Model.Common
{
    //public abstract class BaseEntity
    //{
    //}

    public abstract class Entity<TKey> : IEntity<TKey>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual TKey Id { get; set; }
    }

    public abstract class SimpleEntity : Entity<int>
    {
    }
}