﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace MIFProjects.Support.Model.Common
{
    public interface IContext
    {
        IDbSet<Ticket> Tickets { get; set; }
        IDbSet<Department> Departments { get; set; }
        IDbSet<State> States { get; set; }
        IDbSet<TicketAction> TicketActions { get; set; }
        IDbSet<TicketActionChange> TicketActionChanges { get; set; }
        IDbSet<User> Users { get; set; }

        DbSet<TEntity> Set<TEntity>() where TEntity : class;
        DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;

        int SaveChanges();
    }
}