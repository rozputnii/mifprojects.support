﻿using System.ComponentModel.DataAnnotations;

namespace MIFProjects.Support.Model.Common
{
    public interface IEntity<out TKey>
    {
        TKey Id { get; }
    }
}