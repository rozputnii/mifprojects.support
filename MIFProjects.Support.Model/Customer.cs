﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.RegularExpressions;
using MIFProjects.Support.Model.Common;

namespace MIFProjects.Support.Model
{
    [Table("Customers")]
    public class Customer : SimpleEntity
    {
        public Customer()
        {
            //UrlId = "1";
        }
        //[Required]
        [ScaffoldColumn(false)]
        [MaxLength(256)]
        [Index(IsUnique = true, Order = 0)]
        public string UrlId { get; set; }

        [Required]
        [MaxLength(256)]
        public string CustomerName { get; set; }

        [Required]
        [EmailAddress]
        [MaxLength(256)]
        [Index(IsUnique = true, Order = 1)]
        public string Email { get; set; }

        [InverseProperty("Customer")]
        public ICollection<Ticket> Tickets { get; set; }
    }
}