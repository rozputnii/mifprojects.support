﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MIFProjects.Support.Model.Common;

namespace MIFProjects.Support.Model
{
    [Table("Departments")]
    public class Department : SimpleEntity
    {
        public Department()
        {
            IsActive = true;
        }

        [Required]
        [MaxLength(255)]
        [Display(Name = "Department")]
        [Index(IsUnique = true)]
        public string Name { get; set; }

        [Required]
        public bool IsActive { get; set; }

        [Required]
        public bool IsBase { get; set; }

        [InverseProperty("Department")]
        public ICollection<Ticket> Tickets { get; set; }
    }
}