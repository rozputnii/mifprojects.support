﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MIFProjects.Support.Model.Common;

namespace MIFProjects.Support.Model
{
    [Table("States")]
    public class State : SimpleEntity
    {
        public State()
        {
            IsBase = default(bool);
        }

        [Required]
        [StringLength(255)]
        [Display(Name = "State")]
        [Index(IsUnique = true)]
        public string Name { get; set; }
        
        [Required]
        public bool IsBase { get; set; }
    }
}
