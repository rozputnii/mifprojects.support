﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Microsoft.AspNet.Identity.EntityFramework;
using MIFProjects.Support.Model.Common;

namespace MIFProjects.Support.Model
{
    public class SupportContext : IdentityDbContext<User>, IContext
    {
        public SupportContext()
            : base("Name=SupportContext", throwIfV1Schema: false)
        {
            //this.Configuration.LazyLoadingEnabled = false; 
        }

        public IDbSet<Ticket> Tickets { get; set; }
        public IDbSet<TicketAction> TicketActions { get; set; }
        public IDbSet<TicketActionChange> TicketActionChanges { get; set; }
        public IDbSet<Department> Departments { get; set; }
        public IDbSet<State> States { get; set; }
        public IDbSet<Customer> Customers { get; set; }

        public static SupportContext Create()
        {
            return new SupportContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            base.OnModelCreating(modelBuilder);
        }
    }
}