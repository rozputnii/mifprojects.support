﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Reflection;

namespace MIFProjects.Support.Model
{
    
   
    public class SupportInitializer : DropCreateDatabaseIfModelChanges<SupportContext>
    {
        protected override void Seed(SupportContext context)
        {
            BaseState s = BaseState.WaitingForStaffResponse;

            var states = new List<State>
            {
                new State {Name =  BaseState.WaitingForStaffResponse.GetDescription(), IsBase = true},
                new State {Name = BaseState.WaitingForCustomer.GetDescription()},
                new State {Name = BaseState.OnHold.GetDescription()},
                new State {Name = BaseState.Cancelled.GetDescription()},
                new State {Name = BaseState.Completed.GetDescription()}
            };
            //var states = new List<State>
            //{
            //    new State {Name = "Waiting for Staff Response", IsBase = true},
            //    new State {Name = "Waiting for Customer"},
            //    new State {Name = "On Hold"},
            //    new State {Name = "Cancelled"},
            //    new State {Name = "Completed"}
            //};

            states.ForEach(x => context.States.Add(x));

            var departments = new List<Department>
            {
                new Department {Name = "Sales"},
                new Department {Name = "PM"},
                new Department {Name = "Main", IsBase = true}
            };
            departments.ForEach(x => context.Departments.Add(x));

            context.SaveChanges();
        }
    }
}