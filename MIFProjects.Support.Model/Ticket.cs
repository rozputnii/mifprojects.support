﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.RegularExpressions;
using MIFProjects.Support.Model.Common;

namespace MIFProjects.Support.Model
{
    [Table("Tickets")]
    public class Ticket : SimpleEntity
    {
        public Ticket()
        {
            //todo: on show ticket from url you need convert Date to user local time
            Date = DateTime.UtcNow;
            //UrlId = "1";
        }

        [Required]
        [MaxLength(256)]
        public string Subject { get; set; }

        [ScaffoldColumn(false)]
        [MaxLength(256)]
        [Index(IsUnique = true, Order = 0)]
        public string UrlId { get; set; }

        public string Body { get; set; }

        [Required]
        [ReadOnly(true)]
        [Column(TypeName = "datetime2")]
        public DateTime Date { get; set; }
        
        [Required]
        public int StateId { get; set; }

        [Required]
        public int DepartmentId { get; set; }

        [Required]
        public int CustomerId { get; set; }

        public string OwnerId { get; set; }

        [ForeignKey("DepartmentId")]
        public Department Department { get; set; }

        [ForeignKey("StateId")]
        public State State { get; set; }

        [ForeignKey("CustomerId")]
        public Customer Customer { get; set; }

        [ForeignKey("OwnerId")]
        public User Owner { get; set; }

        [InverseProperty("Ticket")]
        public ICollection<TicketAction> Actions { get; set; }
    }
}