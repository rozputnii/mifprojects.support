using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MIFProjects.Support.Model.Common;

namespace MIFProjects.Support.Model
{
    [Table("TicketActions")]
    public class TicketAction : SimpleEntity
    {
        public TicketAction()
        {
            Date = DateTime.UtcNow;
        }

        [Required]
        [Column(TypeName = "datetime2")]
        public DateTime Date { get; set; }

        [Required]
        public int TicketId { get; set; }

        [Display(Name = "Comment")]
        public string Comment { get; set; }
        
        [Display(Name = "User")]
        public string UserName { get; set; }

        [ForeignKey("TicketId")]
        public virtual Ticket Ticket { get; set; }

        public ICollection<TicketActionChange> Changes { get; set; }
    }
}