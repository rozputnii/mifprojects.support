﻿using System.ComponentModel.DataAnnotations.Schema;
using MIFProjects.Support.Model.Common;

namespace MIFProjects.Support.Model
{
    [Table("TicketActionChanges")]
    public class TicketActionChange : SimpleEntity
    {
        public string PropertyName { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }

        public int TicketActionId { get; set; }

        [ForeignKey("TicketActionId")]
        public virtual TicketAction TicketAction { get; set; }
    }
}