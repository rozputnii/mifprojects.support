﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;
using MIFProjects.Support.Model;
using MIFProjects.Support.Model.Common;

namespace MIFProjects.Support.Service.Common
{
    public abstract class EntityService<TEntity, TKey> : IEntityService<TEntity, TKey> where TKey : IComparable
        where TEntity : class, IEntity<TKey>, new()
    {
        protected readonly IContext Context;
        protected readonly IDbSet<TEntity> Dbset;

        protected EntityService(IContext context)
        {
            Context = context;
            Dbset = Context.Set<TEntity>();
        }

        public virtual TEntity Create(TEntity entity)
        {
            Dbset.Add(Validate(entity));
            Context.SaveChanges();
            return entity;
        }

        public virtual TEntity AddOrUpdate(TEntity entity)
        {
            Dbset.AddOrUpdate(Validate(entity));
            Context.SaveChanges();
            return entity;
        }


        public virtual IEnumerable<TEntity> Filter(Expression<Func<TEntity, bool>> predicate)
        {
            return Query.Where(predicate).AsEnumerable();
        }

        public TEntity Find(Expression<Func<TEntity, bool>> predicate)
        {
            return Query.SingleOrDefault(predicate);
        }

        public virtual void Update(TEntity entity)
        {
            Context.Entry(Validate(entity)).State = EntityState.Modified;
            Context.SaveChanges();
        }

        public virtual void Delete(TEntity entity)
        {
            Dbset.Remove(entity);
            Context.SaveChanges();
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            return Query.AsEnumerable();
        }

        public virtual IQueryable<TEntity> Query => Dbset;

        public virtual TEntity Get(TKey id)
        {
            if (id == null) throw new ArgumentNullException(nameof(id));
            return Dbset.Find(id);
        }

        protected virtual TEntity Validate(TEntity entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));
            return entity;
        }

        public virtual void DeleteById(TKey id)
        {
            var entity = Get(id);
            if (entity != null)
                Delete(entity);
        }
    }
}