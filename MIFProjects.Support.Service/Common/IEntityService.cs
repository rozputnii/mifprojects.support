﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using MIFProjects.Support.Model;
using MIFProjects.Support.Model.Common;

namespace MIFProjects.Support.Service.Common
{
    //<TEntity, TKey> : IEntityService<TEntity> where TEntity : BaseEntity, IEntity<TKey>
    public interface IEntityService<TEntity, in TKey> : IService where TEntity : class, IEntity<TKey>
    {
        IQueryable<TEntity> Query { get; }
        TEntity Get(TKey id);
        TEntity Create(TEntity entity);
        TEntity AddOrUpdate(TEntity entity);
        void Delete(TEntity entity);
        IEnumerable<TEntity> GetAll();
        IEnumerable<TEntity> Filter(Expression<Func<TEntity, bool>> predicate);
        TEntity Find(Expression<Func<TEntity, bool>> predicate);
        void Update(TEntity entity);
    }
}