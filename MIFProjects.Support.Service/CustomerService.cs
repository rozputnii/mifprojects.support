using System;
using System.Data.Entity;
using System.Linq;
using System.Text.RegularExpressions;
using MIFProjects.Support.Model;
using MIFProjects.Support.Model.Common;
using MIFProjects.Support.Service.Common;

namespace MIFProjects.Support.Service
{
    public class CustomerService : EntityService<Customer, int>, ICustomerService
    {
        public CustomerService(IContext context) : base(context)
        {
        }

        protected override Customer Validate(Customer entity)
        {
            entity = base.Validate(entity);
            entity.Email = entity.Email.ToLower().Trim();
            return entity;
        }
        
        public virtual Customer GetByEmail(string email)
        {
            email = email?.ToLower().Trim() ?? throw new ArgumentNullException(nameof(email));
            return Query.Include(o => o.Tickets).SingleOrDefault(o => o.Email==email);
        }

        public Customer GetByUrl(string url)
        {
            if(url==null)
                throw new ArgumentNullException(nameof(url));

            var customer = Query.SingleOrDefault(o => o.UrlId==url);
            //if (customer != null && tickets)
            //{
            //    Context.Entry(customer).Collection(o=>o.Tickets).Load();
            //}
            return customer;
        }

        public override Customer Get(int id)
        {
            var customer =  Query.Include(o=>o.Tickets).SingleOrDefault(o => o.Id == id);
            if (customer != null)
            {
                foreach (var ticket in customer.Tickets)
                {
                    Context.Entry(ticket).Reference(o => o.Department).Load();
                    Context.Entry(ticket).Reference(o => o.Customer).Load();
                    Context.Entry(ticket).Reference(o => o.Owner).Load();
                    Context.Entry(ticket).Reference(o => o.State).Load();
                }
            }
            return customer;
        }

        public override Customer AddOrUpdate(Customer entity)
        {
            var customer = GetByEmail(entity.Email);
            if (customer != null)
            {
                entity.Id = customer.Id;
                entity.UrlId = customer.UrlId;
            }
            else
            {
                entity.UrlId = Regex.Replace(Convert.ToBase64String(Guid.NewGuid().ToByteArray()), @"[/+=]", "");
            }
            return base.AddOrUpdate(entity);
        }

        public override Customer Create(Customer entity)
        {
            entity.UrlId = Regex.Replace(Convert.ToBase64String(Guid.NewGuid().ToByteArray()), @"[/+=]", "");
            return base.Create(entity);
        }
    }
}