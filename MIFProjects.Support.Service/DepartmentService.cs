﻿using System.Linq;
using MIFProjects.Support.Model;
using MIFProjects.Support.Model.Common;
using MIFProjects.Support.Service.Common;

namespace MIFProjects.Support.Service
{
    public class DepartmentService : EntityService<Department, int>, IDepartmentService
    {
        public DepartmentService(IContext context) : base(context)
        {
        }
        public virtual Department Base => Dbset.SingleOrDefault(o => o.IsBase);
    }
}