using MIFProjects.Support.Model;
using MIFProjects.Support.Service.Common;

namespace MIFProjects.Support.Service
{
    public interface ICustomerService : IEntityService<Customer, int>
    {
        Customer GetByEmail(string email);
        Customer GetByUrl(string url);
    }
}