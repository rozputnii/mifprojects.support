using MIFProjects.Support.Model;
using MIFProjects.Support.Service.Common;

namespace MIFProjects.Support.Service
{
    public interface IDepartmentService : IEntityService<Department, int>
    {
        Department Base { get; }
    }
}