using MIFProjects.Support.Model;
using MIFProjects.Support.Service.Common;

namespace MIFProjects.Support.Service
{
    public interface IStateService : IEntityService<State, int>
    {
        State Base { get; }
    }
}