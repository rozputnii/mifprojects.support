﻿using MIFProjects.Support.Model;
using MIFProjects.Support.Service.Common;

namespace MIFProjects.Support.Service
{
    public interface ITicketActionChangeService : IEntityService<TicketActionChange, int>
    {
    }
}