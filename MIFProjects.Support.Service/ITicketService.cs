﻿using System.Collections.Generic;
using MIFProjects.Support.Model;
using MIFProjects.Support.Service.Common;

namespace MIFProjects.Support.Service
{
    public interface ITicketService : IEntityService<Ticket, int>
    {
        IEnumerable<Ticket> FindByEmail(string email);
        IEnumerable<Ticket> FindBySubject(string subject);
        Ticket GetByUrl(string urlId);
    }
}