using System.Collections;
using System.Collections.Generic;
using MIFProjects.Support.Model;
using MIFProjects.Support.Service.Common;

namespace MIFProjects.Support.Service
{
    public interface IUserService : IEntityService<User, string>
    {
        User UserWithTickets(string id);
    }
}