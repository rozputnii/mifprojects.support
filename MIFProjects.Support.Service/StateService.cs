using System.Linq;
using MIFProjects.Support.Model;
using MIFProjects.Support.Model.Common;
using MIFProjects.Support.Service.Common;

namespace MIFProjects.Support.Service
{
    public class StateService : EntityService<State, int>, IStateService
    {
        public StateService(IContext context) : base(context)
        {
        }

        public virtual State Base => Dbset.SingleOrDefault(o => o.IsBase);
    }
}