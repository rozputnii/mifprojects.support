using MIFProjects.Support.Model;
using MIFProjects.Support.Model.Common;
using MIFProjects.Support.Service.Common;

namespace MIFProjects.Support.Service
{
    public class TicketActionChangeService : EntityService<TicketActionChange, int>, ITicketActionChangeService
    {
        public TicketActionChangeService(IContext context) : base(context)
        {
        }
    }
}