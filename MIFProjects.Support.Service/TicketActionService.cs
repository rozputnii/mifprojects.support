﻿using MIFProjects.Support.Model;
using MIFProjects.Support.Model.Common;
using MIFProjects.Support.Service.Common;

namespace MIFProjects.Support.Service
{
    public class TicketActionService : EntityService<TicketAction, int>, ITicketActionService
    {
        public TicketActionService(IContext context) : base(context)
        {
        }
    }
}