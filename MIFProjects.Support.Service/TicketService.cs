﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using MIFProjects.Support.Model;
using MIFProjects.Support.Model.Common;
using MIFProjects.Support.Service.Common;

namespace MIFProjects.Support.Service
{
    public class TicketService : EntityService<Ticket, int>, ITicketService
    {
        public TicketService(IContext context) : base(context)
        {
        }

        public override IQueryable<Ticket> Query => base.Query
            .Include(o => o.Customer)
            .Include(o => o.Owner)
            .Include(o => o.Department)
            .Include(o => o.State);

        public override Ticket Create(Ticket entity)
        {
            if (entity.StateId == default(int))
            {
                var state = Context.States.SingleOrDefault(o => o.IsBase);
                if (state != null)
                {
                    entity.StateId = state.Id;
                }
            }
            if (entity.DepartmentId == default(int))
            {
                var department = Context.Departments.SingleOrDefault(o => o.IsBase);
                if (department != null)
                {
                    entity.DepartmentId = department.Id;
                }
            }

            entity.UrlId = Regex.Replace(Convert.ToBase64String(Guid.NewGuid().ToByteArray()), @"[/+=]", "");
            return base.Create(entity);
        }

        public virtual Ticket GetByUrl(string urlId)
        {
            return Dbset.SingleOrDefault(x => x.UrlId == urlId);
        }

        public virtual IEnumerable<Ticket> FindBySubject(string subject)
        {
            return Filter(x => x.Subject.StartsWith(subject));
        }

        public override Ticket Get(int id)
        {
            return Query.Include(o=>o.Actions).SingleOrDefault(o => o.Id == id);
        }

        public virtual IEnumerable<Ticket> FindByEmail(string email)
        {
            email = email.ToLower().Trim();
            return Query.Where(x => x.Customer.Email == email).AsEnumerable();
        }
    }
}