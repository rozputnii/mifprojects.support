using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using MIFProjects.Support.Model;
using MIFProjects.Support.Model.Common;
using MIFProjects.Support.Service.Common;

namespace MIFProjects.Support.Service
{
    public class UserService : EntityService<User, string>, IUserService
    {
        public UserService(IContext context) : base(context)
        {
        }

        public override IQueryable<User> Query => base.Query.Include(o => o.Department);

        public User UserWithTickets(string id)
        {
            var user = Query.Include(o=>o.Tickets).SingleOrDefault(o => o.Id == id);
            return user;
        }
    }
    
}