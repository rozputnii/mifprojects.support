﻿using System.Linq;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using MIFProjects.Support.Model;
using MIFProjects.Support.Service;

namespace MIFProjects.Support.Controllers
{
    [Authorize]
    public abstract class BaseAdminController : Controller
    {
        protected readonly ApplicationUserManager UserManager;
        protected readonly IUserService UserService;

        protected BaseAdminController(ApplicationUserManager userManager, IUserService userService)
        {
            UserManager = userManager;
            UserService = userService;
        }

        protected virtual User GetUser()
        {
            if (!User.Identity.IsAuthenticated) return null;
            var uId = User.Identity.GetUserId();
            var user = UserService.Get(uId);
            return user;
        }

        protected virtual bool IsAdminUser
        {
            get
            {
                var user = GetUser();
                if (user == null) return false;
                var s = UserManager.GetRoles(user.Id);
                return s.Any() && s[0] == "Admin";
            }
        }
    }

    [Authorize]
    public class AdminController : BaseAdminController
    {
        protected readonly IdentityDbContext<User> Context;

        

        /// <summary>
        ///     Get All Roles
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            if (!IsAdminUser)
                return RedirectToAction("Login", "Account");

            return RedirectToAction("Index", "Users");
        }

        public ActionResult Roles()
        {
            if (!IsAdminUser)
                return RedirectToAction("Index");
            var roles = Context.Roles.AsEnumerable();
            return View(roles);
        }

        /// <summary>
        ///     Create  a New role
        /// </summary>
        /// <returns></returns>
        public ActionResult Create()
        {
            if (!IsAdminUser)
                return RedirectToAction("Index");

            return View(new IdentityRole());
        }

        /// <summary>
        ///     Create a New Role
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(IdentityRole role)
        {
            if (!IsAdminUser)
                return RedirectToAction("Index");


            Context.Roles.Add(role);
            Context.SaveChanges();
            return RedirectToAction("Roles");
        }

        public AdminController(ApplicationUserManager userManager, IUserService userService) : base(userManager, userService)
        {
        }
    }
}