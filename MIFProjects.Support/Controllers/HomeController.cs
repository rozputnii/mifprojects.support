﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MIFProjects.Support.Service;

namespace MIFProjects.Support.Controllers
{
    [AllowAnonymous]
    public class HomeController : BaseAdminController
    {
        private readonly ICustomerService _customerService;
        private readonly ITicketService _ticketService;

        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", IsAdminUser ? "Admin" : "Tickets");
            }
            
            return RedirectToAction("Create", "Tickets");
        }

        public HomeController(ApplicationUserManager userManager, IUserService userService, ICustomerService customerService, ITicketService ticketService) : base(userManager, userService)
        {
            _customerService = customerService;
            _ticketService = ticketService;
        }
        }
}