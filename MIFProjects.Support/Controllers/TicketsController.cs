﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using MIFProjects.Support.Model;
using MIFProjects.Support.Models;
using MIFProjects.Support.Service;

namespace MIFProjects.Support.Controllers
{
    [AllowAnonymous]
    public class TicketsController : BaseAdminController
    {
        private readonly ICustomerService _customerService;
        private readonly IDepartmentService _departmentService;
        private readonly IStateService _stateService;
        private readonly ITicketService _ticketService;
        private readonly IUserService _userService;

        private readonly ITicketActionService _actionService;
        // GET: Tickets/Edit/5
        //public ActionResult Edit(int? id)
        //{
        //    if (id == null)
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    Ticket ticket = db.Tickets.Find(id);
        //    if (ticket == null)
        //        return HttpNotFound();
        //    ViewBag.CustomerId = new SelectList(db.Customers, "Id", "CustomerName", ticket.CustomerId);
        //    ViewBag.DepartmentId = new SelectList(db.Departments, "Id", "Name", ticket.DepartmentId);
        //    ViewBag.OwnerId = new SelectList(db.ApplicationUsers, "Id", "Email", ticket.OwnerId);
        //    ViewBag.StateId = new SelectList(db.States, "Id", "Name", ticket.StateId);
        //    return View(ticket);
        //}

        //// POST: Tickets/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit(
        //    [Bind(Include = "Id,Subject,Body,StateId,DepartmentId,CustomerId,OwnerId")] Ticket ticket)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(ticket).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.CustomerId = new SelectList(db.Customers, "Id", "CustomerName", ticket.CustomerId);
        //    ViewBag.DepartmentId = new SelectList(db.Departments, "Id", "Name", ticket.DepartmentId);
        //    ViewBag.OwnerId = new SelectList(db.ApplicationUsers, "Id", "Email", ticket.OwnerId);
        //    ViewBag.StateId = new SelectList(db.States, "Id", "Name", ticket.StateId);
        //    return View(ticket);
        //}

        //// GET: Tickets/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    Ticket ticket = db.Tickets.Find(id);
        //    if (ticket == null)
        //        return HttpNotFound();
        //    return View(ticket);
        //}

        //// POST: Tickets/Delete/5
        //[HttpPost]
        //[ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    Ticket ticket = db.Tickets.Find(id);
        //    db.Tickets.Remove(ticket);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}
        public TicketsController(ApplicationUserManager userManager, IUserService userService,
            IUserService userService1, ITicketService ticketService, IStateService stateService,
            IDepartmentService departmentService, ICustomerService customerService, ITicketActionService actionService) : base(userManager, userService)
        {
            _userService = userService1;
            _ticketService = ticketService;
            _stateService = stateService;
            _departmentService = departmentService;
            _customerService = customerService;
            _actionService = actionService;
        }



        // GET: Tickets
        [Authorize]
        public ActionResult Index()
        {
            var userId = User.Identity.GetUserId();
            var user = _userService.Get(userId);
            if (user == null)
            {
                return RedirectToAction("Login", "Account");
            }
            var state = _stateService.Base;
            var tickets = _ticketService.Filter(o => o.StateId == state.Id && o.OwnerId == null);
            tickets = tickets.OrderByDescending(o => o.Date);
            var enumerable = tickets as IList<Ticket> ?? tickets.ToList();
            foreach (var ticket in enumerable)
            {
                ticket.Date = ticket.Date.ToLocalTime();
            }
            return View(enumerable);
        }

        [Authorize]
        public ActionResult MyTickets()
        {
            var userId = User.Identity.GetUserId();
            var user = _userService.Get(userId);
            if (user == null)
            {
                return RedirectToAction("Login", "Account");
            }
            var tickets = _ticketService.Filter(o => o.OwnerId == user.Id);
            tickets = tickets.OrderByDescending(o => o.Date);
            var enumerable = tickets as IList<Ticket> ?? tickets.ToList();
            foreach (var ticket in enumerable)
            {
                ticket.Date = ticket.Date.ToLocalTime();
            }
            return View(enumerable);
        }

        //public ActionResult Customer()
        //{
            
        //}


        [Authorize]
        public ActionResult AssignToMe(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var ticket = _ticketService.Get(id.Value);
            if (ticket == null)
                return HttpNotFound();
            var user = GetUser();
            ticket.OwnerId = user.Id;
            _ticketService.Update(ticket);
            return RedirectToAction("Details", new {id=ticket.Id});
        }

        [AllowAnonymous]
        public ActionResult Details(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var ticket = _ticketService.Get(id.Value);
            if (ticket == null)
                return HttpNotFound();
            ViewBag.CommentVm = new TicketActionViewModel {TicketId = ticket.Id};
            return View(ticket);
        }
        [Authorize]
        public ActionResult ChangeState(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var ticket = _ticketService.Get(id.Value);
            if (ticket == null)
                return HttpNotFound();
            ViewBag.StateId = new SelectList(_stateService.GetAll(), "Id", "Name", ticket.StateId);
            var vm = new TicketStateViewModel {Id = ticket.Id, StateId = ticket.StateId};
            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public ActionResult AddComment(TicketActionViewModel vm)
        {
            var ticket = _ticketService.Get(vm.TicketId);
            if (ModelState.IsValid)
            {
                string userName = "";

                User user = null;
                if (User.Identity.IsAuthenticated)
                {
                    
                    var userId = User.Identity.GetUserId();
                    user = _userService.Get(userId);
                }

                if (user != null)
                {
                    userName = user.UserName;
                    string name = BaseState.WaitingForCustomer.GetDescription();
                    var state = _stateService.Find(o => o.Name== name);
                    if (state != null)
                    {
                        ticket.StateId = state.Id;
                        _ticketService.Update(ticket);
                    }
                }
                else
                {
                    userName = ticket.Customer.CustomerName;
                    string name = BaseState.WaitingForStaffResponse.GetDescription();
                    var state = _stateService.Find(o => o.Name == name);
                    if (state != null)
                    {
                        ticket.StateId = state.Id;
                        _ticketService.Update(ticket);
                    }
                }
                var action = new TicketAction {TicketId = ticket.Id, Comment = vm.Comment, UserName = userName};

                _actionService.Create(action);
            }
            return RedirectToAction("Details", new {id = ticket.Id});
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult ChangeState(TicketStateViewModel vm)
        {
            if (ModelState.IsValid)
            {
                var old =_ticketService.Get(vm.Id);
                old.StateId = vm.StateId;
                _ticketService.Update(old);
                return RedirectToAction("Details", new {id= old.Id});
            }
            ViewBag.StateId = new SelectList(_stateService.GetAll(), "Id", "Name", vm.StateId);
            return View(vm);
        }

        [AllowAnonymous]
        public ActionResult ByUrl(string id)
        {
            if (id != null)
            {
                var url = id;
                var customer = _customerService.GetByUrl(url);
                if (customer != null)
                {
                    return RedirectToAction("ByCustomer", new { id = customer.Id });
                }
                var ticket = _ticketService.GetByUrl(url);
                if (ticket != null)
                {
                    return RedirectToAction("Details", new { id = ticket.Id });
                }
                return HttpNotFound();
            }
            //return HttpNotFound();
            return RedirectToAction("Index", "Home");
        }

        [AllowAnonymous]
        public ActionResult ByCustomer(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var customer = _customerService.Get(id.Value);
            if (customer == null)
                return HttpNotFound();
            return View(customer);
        }

        // GET: Tickets/Create

        [AllowAnonymous]
        public ActionResult Create()
        {
            ViewBag.DepartmentId = new SelectList(_departmentService.GetAll(), "Id", "Name", _departmentService.Base.Id);
            return View();
        }

        // POST: Tickets/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public ActionResult Create(
            [Bind(Include = "Id,Subject,Body,DepartmentId")] Ticket ticket,
            [Bind(Include = "Email,CustomerName")] Customer customer)
        {
           
            if (ModelState.IsValid)
            {
                customer = _customerService.AddOrUpdate(customer);
                ticket.CustomerId = customer.Id;
                _ticketService.Create(ticket);
                Console.WriteLine(ticket.Id);
                var client = new SmtpClient();
                
                var mail = new MailMessage("mifprojects@mifsupport.com",customer.Email);
                mail.Subject = $"You have create a ticket: {ticket.Subject}";
                var tUrl = Request.Url.Scheme + "://" + Request.Url.Host + ":" + Request.Url.Port +
                           Url.Action("ByUrl", "Tickets", new {id = ticket.UrlId});
                var cUrl = Request.Url.Scheme + "://" + Request.Url.Host + ":" + Request.Url.Port +
                           Url.Action("ByUrl", "Tickets", new { id = customer.UrlId });
                var body = $"You may show ticket on: {tUrl}\nYou tickets list here: {cUrl}";

                mail.Body = body;
                client.Send(mail);
                return RedirectToAction("Details", new { id = ticket.Id });
            }
            ViewBag.DepartmentId = new SelectList(_departmentService.GetAll(), "Id", "Name", ticket.DepartmentId);
            return View(ticket);
        }
    }
}