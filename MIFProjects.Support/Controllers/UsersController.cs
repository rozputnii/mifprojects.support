﻿using System.Net;
using System.Web.Mvc;
using MIFProjects.Support.Model;
using MIFProjects.Support.Service;

//Microsoft.AspNet.Identity.EntityFramework
namespace MIFProjects.Support.Controllers
{
    [Authorize]
    public class UsersController : BaseAdminController
    {
       
        // GET: Users
        public ActionResult Index()
        {
            if (!IsAdminUser)
            {
                return RedirectToAction("Login", "Account");
            }
            var users = UserService.GetAll();
            return View(users);
         }

        // GET: ApplicationUsers/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = UserService.Get(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = UserService.Get(id);
            if (user == null)
                return HttpNotFound();

            var departments = DepartmentService.GetAll();
            ViewBag.DepartmentId = new SelectList(departments, "Id", "Name", user.DepartmentId);
            return View(user);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,DepartmentId,Email,EmailConfirmed,PasswordHash,SecurityStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,LockoutEndDateUtc,LockoutEnabled,AccessFailedCount,UserName")] User user)
        {
            if (ModelState.IsValid)
            {
                UserService.Update(user);
                return RedirectToAction("Index");
            }
            var departments = DepartmentService.GetAll();
            ViewBag.DepartmentId = new SelectList(departments, "Id", "Name", user.DepartmentId);
            return View(user);
        }

        protected virtual IDepartmentService DepartmentService { get; }

        public UsersController(ApplicationUserManager userManager, IUserService userService, IDepartmentService departmentService) : base(userManager, userService)
        {
            DepartmentService = departmentService;
        }
    }
}