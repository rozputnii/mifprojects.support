﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MIFProjects.Support.Models
{
    public class TicketStateViewModel
    {
        [Required]
        [ScaffoldColumn(false)]
        public int Id { get; set; }

        [Required]
        [ScaffoldColumn(false)]
        [Display(Name="State")]
        public int StateId { get; set; }

    }

    public class TicketActionViewModel
    {
        [Required]
        [ScaffoldColumn(false)]
        public int TicketId { get; set; }

        [Display(Name = "Comment")]
        public string Comment { get; set; }
    }
}