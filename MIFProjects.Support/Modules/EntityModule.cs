﻿using System.Data.Entity;
using Autofac;
using Microsoft.AspNet.Identity.EntityFramework;
using MIFProjects.Support.Model;
using MIFProjects.Support.Model.Common;
using MIFProjects.Support.Models;

namespace MIFProjects.Support.Modules
{
    public class EntityModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //builder.RegisterType(typeof(ApplicationDbContext)).As(typeof(DbContext)).InstancePerLifetimeScope();
            builder.RegisterType(typeof(SupportContext)).As(typeof(IContext)).InstancePerLifetimeScope();
            builder.RegisterType(typeof(SupportContext)).As(typeof(IdentityDbContext<User>)).InstancePerLifetimeScope();
        }
    }

}